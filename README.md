# Firefox-Privacy

This script can be used to maintain firefox's configurations updated. Configurations parameters come from the website https://www.privacytools.io/
This script has been made using python 3.

# Manual for Windows
- Use keys Windows + R then type in the path section %appdata%\mozilla\firefox\profiles\
- Enter into one of your profile's directory
- Copy the document `prefs.js` to the folder when the script is located
- Execute the script (if you don't have python installed there is a pre compiled file)
- Replace the document `prefs.js` into the right folder

# Manual for Linux
- Go into the directory `/home/<user>/.mozilla/firefox/profiles/`
- Enter into one of your profile's directory
- Copy the document `prefs.js` to the folder when the script is located
- Execute the script
- Replace the document `prefs.js` into the right folder

# Future update
Offer to select the right firefox's profile with the script.

# Legal purpose
You can use, modify and share the script but try to give your sources ;-)
