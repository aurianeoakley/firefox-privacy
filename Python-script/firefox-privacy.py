#!/usr/bin/python3
"""

Firefox Configuration Python

"""
import urllib.request
from time import sleep
import platform
from os import listdir, getlogin
from tkinter import *

###### important vars ######
parameters=['parameters:']
config = ['config:']
string = ""
writed = False
path = ""

def chooseProfile(number):
	path="c:/users/"+getlogin()+"/appdata/roaming/mozilla/firefox/profiles/"+profiles[number]+"/prefs.js"
	config(path)

##### configuration of prefs.js #####

def config(path):
	Text3 = Text(window, height=2, width=30)
	Text3.pack()
	Text3.insert(END, "[*] Getting information from the web (privacytools.io) ...")
	with urllib.request.urlopen('https://www.privacytools.io/browsers/') as response:
		html = response.read()
	html = html.decode('utf-8')
	lst = html.splitlines()
	for i in range(0,len(lst)):
		if '<dt>' in lst[i]:
			parameters.append(lst[i])
		else:
			continue
	Text4 = Text(window, height=2, width=30)
	Text4.pack()
	Text4.insert(END, "[*] Parsing the html code ...")
	for i in range(0, len(parameters)):
		parameters[i]=parameters[i].replace("  <dt>","")
		parameters[i]=parameters[i].replace("</dt>", "")

	with open(path) as file:
		data = file.read()
	file.closed

	Text5 = Text(window, height=2, width=30)
	Text5.pack()
	Text5.insert(END, "[*] Writing config in file ...")

	with open(path, 'w') as file:
		for i in range(1,len(parameters)):
			writed=False
			for line in data.splitlines():
				if parameters[i].split(" = ")[0] in line:
					string = "user_pref(\"" + parameters[i].split(" = ")[0] + "\"," + parameters[i].split(" = ")[1] + ");"
					file.write(string+"\n")
					writed = True
					break
				else:
					continue

			if(writed != True):
				file.write("user_pref(\"" + parameters[i].split(" = ")[0] + "\"," + parameters[i].split(" = ")[1] + ");"+"\n")
			else:
				continue
	file.closed
	Text6 = Text(window, height=2, width=30)
	Text6.pack()
	Text6.insert(END, "[*] Done \nYou can close the window")


###### GUI ######
window = Tk()
Text1 = Text(window, height=2, width=30)
Text1.pack()
Text1.insert(END, "[*] Starting the configuration software ...")
Text2 = Text(window, height=2, width=30)
Text2.pack()
Text2.insert(END, "Choose profile")

###### Os Specs ######
if("Windows" in platform.platform()):
	profiles=listdir("c:\\users\\"+getlogin()+"\\appdata\\roaming\\mozilla\\firefox\\profiles\\")
else:
	profiles=listdir("/home/"+getlogin()+"/.mozilla/firefox/profiles/")

button_list=["0"]
#Configuring the length of the button list
for i in range(1,len(profiles)):
	button_list = [*button_list, "0"]
for i in range(0,len(profiles)):
	button_list=Button(window, text=profiles[i], command=lambda x=i:chooseProfile(x))
	button_list.pack()

#### MAIN ####
window.mainloop()
print("[*] Exiting ...")
sleep(2)

